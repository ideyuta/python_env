## 環境構築
pyenvとvenvを組み合わせることで、任意のバージョンのpythonで仮想環境を構築する.  
### 手順
1. ローカル環境にて、pyenvで任意のpythonのバージョンに切り替える.  
1. venvで仮想環境を構築する.  

※仮想環境内のpythonのバージョンは、仮想環境から一度抜けたり、ローカル環境のpythonのバージョンを切り替えても変わることはなく、ずっと環境構築時に指定したバージョンが維持される。
### 具体例
```
# 手順1
pyenv local 3.10.6
# 手順2
python -m venv --upgrade-deps test
# 仮想環境に入って作業
source test/bin/activate
# 仮想環境から出てローカル環境に戻る
deactivate
```

## pyenv
複数のpythonのバージョンを切り替えて使用するためのツール.  
INFO: 設定ができていないので現状は、`pyenv shell [version]`は使えない。  
[参考サイト](https://ai-can-fly.hateblo.jp/entry/how-to-use-pyenv)  
|Command|Description|  
|----|----|  
|pyenv install --list|pyenv でインストール可能な Python のバージョンを一覧表示|  
|pyenv install [version]|指定したバージョンの Python をインストール|
|pyenv versions|pyenv でインストールした Python を一覧表示|
|pyenv shell [version]|現在のシェルで使用する Python のバージョンを設定|
|pyenv local [version]|カレントディレクトリ以下で使用する Python のバージョンを設定|
|pyenv global [version]|すべてのシェルで使用する Python のバージョンを設定|
|pyenv version|現在設定されている Python のバージョンを表示|
|pyenv uninstall [version]|指定したバージョンの Python をアンインストール|

## venv
python標準の仮想環境構築コマンド.  
仮想環境毎にモジュールが完全に切り分けて管理されるため、(pip listすると分かるが)初期状態ではほとんどモジュールがインストールされていない.  
### 仮想環境を生成
```
python -m venv --upgrade-deps <仮想環境名>
```
### アクティベート(仮想環境に入る)
```
source <仮想環境名>/bin/activate
```
### ディアクティベート(仮想環境から出る)
```
deactivate
```

## このリポジトリのユースケース
pythonを使った別のリポジトリをクローンしてきてサクッと動かしたい.  
※クローンするリポジトリ側ではpython環境を構築するスクリプト等が準備されておらず、ローカルで構築する必要がある場合に有用.

### 手順
以下の手順を踏むことで、指定したpythonバージョンで仮想環境を構築し、クローンしてきたリポジトリで必要なパッケージをインストールできる.  
※dockerコンテナを再生成した場合は、ステップ3以降を再度実施すること.
```
1. リポジトリをvolume配下にクローン
cd volume
git clone <リポジトリURL>
cd ..

2. dockerビルド
docker compose up -d --build

3. dockerコンテナに入る
docker exec -it py /bin/bash

4. 任意のバージョンのpythonをインストール
pyenv install <pythonバージョン>

5. 使用するpythonのバージョンを設定
pyenv local <pythonバージョン>

6. pythonの仮想環境を生成
python -m venv --upgrade-deps <仮想環境名>

7. 仮想環境に入る
source <仮想環境名>/bin/activate

8. 必要なpythonパッケージをインストール
※クローンしたリポジトリのどこかにrequirements.txtが存在する想定
./requirements.sh install <requirements.txtがあるディレクトリ> 
(ex) ./requirements.sh install .
```
