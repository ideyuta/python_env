FROM ubuntu:22.04  

WORKDIR /

# Bashを使う
SHELL ["/bin/bash", "-c"]
# インストール中に入力を求められるのを防止
RUN DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    tzdata
# pyenvに必要なもの等
RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    git \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    wget \
    curl \
    llvm \
    libncursesw5-dev \
    xz-utils \
    tk-dev \
    libxml2-dev \
    libxmlsec1-dev \
    libffi-dev \
    liblzma-dev \
    ca-certificates \
    sudo
# pyenvインストール&設定
ENV PYENV_ROOT=/root/.pyenv
ENV PATH=${PYENV_ROOT}/bin:${PYENV_ROOT}/shims:$PATH
RUN export GIT_SSL_NO_VERIFY=1 && \
    git clone https://github.com/pyenv/pyenv.git /root/.pyenv
RUN cd /root/.pyenv && src/configure && make -C src
# pipで使用しているパッケージ情報(requirements.txt管理用スクリプト
COPY requirements.sh /requirements.sh
RUN chmod +x /requirements.sh
# Entrypoint
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/bin/bash", "-c", "/entrypoint.sh; /bin/bash;"]
