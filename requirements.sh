#!/usr/bin/env bash
set -ue

SCRIPT_PATH=${BASH_SOURCE:-$0}
SCRIPT_DIR=$(cd $(dirname ${SCRIPT_PATH}); pwd)
REQUIREMENTS_DIR=${SCRIPT_DIR}

# =====================================================================================
# Extract args
CMD=${1:-}
if [ "$#" -ge 2 ]; then
  REQUIREMENTS_DIR=${2}
fi
shift 2
# REQUIREMENTS_DIRの末尾の/(スラッシュ)を除去
REQUIREMENTS_DIR=$(echo "${REQUIREMENTS_DIR}" | sed 's:/*$::')
REQUIREMENTS_PATH=${REQUIREMENTS_DIR}/requirements.txt

# =====================================================================================
# Help
function print_help() {
cat << EOM
./$(basename ${SCRIPT_PATH}) export .
./$(basename ${SCRIPT_PATH}) install .
EOM
  exit 1
}

# =====================================================================================
# Export modules to requirements.txt
function _export() {
  pip freeze > ${REQUIREMENTS_PATH}
}

# =====================================================================================
# Install modules from requirements.txt
function _install() {
  pip install -r ${REQUIREMENTS_PATH}
}

# =====================================================================================
# Execute command
if [ "$(type -t _$CMD)" == "function" ]; then
  _$CMD $@
else
  print_help
fi
